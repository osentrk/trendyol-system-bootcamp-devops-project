# Sanal makinada bir Centos kurup, güncellemelerinin yapılması

Oracle VirtualBox ile Centos 7 imajı kullanarak bir adet sanal makine oluşturdum ve kurulumunu gerçekleştirdim. sudo yum -y update komutu ile makinede bulunan tüm paketleri güncelleştirdim.

# Kişisel bir user yaratılması (ad.soyad şeklinde)

adduser oguzhan.senturk ile kendime yeni bir user oluşturdum ve şifresini passwd oguzhan.senturk komutuyla belirledim. Ardından visudo komutunu çalıştırdım ve root kullanıcının ALL=(ALL) yapıldığı satırın altına oguzhan.senturk ALL=(ALL) ALL yazdım ve dosyayı kaydettim. Bu işlem ile oguzhan.senturk kullanıcısına root yetkisi tanımladım.

# Sunucuya 2. disk olarak 10 GB disk eklenmesi ve "bootcamp" olarak mount edilmesi  

VirtualBox'da makinemin ayarları kısmından bir adet 10 GB'lık VDİ disk oluşturdum. Centos üzerinde önce dif komutu ile mevcut mount edilmiş diskleri kontrol ettim. ardından fdisk -l ile disklerime baktım ve Ayarlar kısmında eklediğim diskin adını öğrendim. Adının /dev/sdb olduğunu gördüm ve bu disk üzerinde fdisk /dev/sdb komutu ile bir partition oluşturdum. Ardından mkfs.ext4 /dev/sdb ile diski formatladım ve ana dizinde mkdir bootcamp komutu ve mount /dev/sdb /bootcamp komutu ile diski mount ettim.

# /opt/bootcamp/ altında bootcamp.txt diye bir file yaratılıp, file'ın içerisine "merhaba trendyol"  yazılması  

İlgili dizine cd /opt/bootcamp/ ile ulaştıktan sonra touch bootcamp.txt ve echo 'merhaba trendyol' > bootcamp.txt komutları ile dosyanın içerisine merhaba trendyol yazısını yazdırdım.

# Kişisel kullanıcının home dizininde tek bir komut kullanarak bootcamp.txt file'ını bulup, bootcamp  diskine taşınması 

find -name "bootcamp.txt" -exec cp {} /bootcamp \;
