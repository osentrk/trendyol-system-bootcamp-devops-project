# Ansible kurulması

sudo yum install ansible

# Docker Playbook Oluşturulması ve Çalıştırılması

mkdir pbooks
cd pbooks

vi pbook_main.yaml

ansible-playbook pbook_main.yaml

# Sunucu Playbook

vi pbook.yaml

ansible-playbook pbook.yaml

# Dockerın durumunu kontrol etme

docker ps


# Hoş Geldin Devops Sayfası

vi devops.html
